using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;


namespace ElementInvoiceEmails
{
    public static class Progam
    {
        public enum Vendor { NONE = 0, TOPCO = 162, GREGG = 87, BADGER = 875, MOTIONCANADA = 239, SPM = 22, MRDIESEL =  373};

        static string[] Scopes = { GmailService.Scope.GmailLabels, GmailService.Scope.GmailModify };

        static string fileFolder = @"c:\dl\";

        public static void Main()
        {
            UserCredential credential;

            try
            {

                using (var stream =
                    new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
                {
                    // The file token.json stores the user's access and refresh tokens, and is created
                    // automatically when the authorization flow completes for the first time.
                    string credPath = "token.json";
                    credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        GoogleClientSecrets.Load(stream).Secrets,
                        Scopes,
                        "user",
                        CancellationToken.None,
                        new FileDataStore(credPath, true)).Result;
                    Console.WriteLine("Credential file saved to: " + credPath);

                }

                // Create Gmail API service.
                var service = new GmailService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "Invoice Checker",
                });

                string userID = ConfigurationManager.AppSettings["mailbox"];

                Console.WriteLine("Mailbox: " + userID);


                int countryID = Convert.ToInt32(ConfigurationManager.AppSettings["countryID"]);





                var labelList = service.Users.Labels.List("me").Execute();
                var labelID = "";
                var unreadLabelID = "";
                IList<string> labels = new List<string>();
                IList<string> removeLabels = new List<string>();

                foreach (var label in labelList.Labels)
                {
                    if (label.Name == "EBS Invoice")
                        labelID = label.Id;

                    else if (label.Name == "UNREAD")
                        unreadLabelID = label.Id;
                }

                labels.Add(labelID);
                removeLabels.Add(unreadLabelID);

                while (1 == 1)
                {

                    var request = service.Users.Messages.List("me");
                    request.Q = "has:nouserlabels";

                    var executedReq = request.Execute();

                    Console.WriteLine("Requested messages");

                    if (executedReq != null && executedReq.Messages != null)
                    {

                        foreach (var msg in executedReq.Messages)//.Where(x=>x.Payload.Headers.Contains)
                        {
                            string subject = "";

                            try
                            {

                                var bigMsg = service.Users.Messages.Get("me", msg.Id).Execute();

                                DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeMilliseconds(Convert.ToInt64(bigMsg.InternalDate));

                                DateTime dateSent = dateTimeOffset.DateTime;

                                subject = bigMsg.Payload.Headers.Where(x => x.Name.ToLower() == "subject").FirstOrDefault().Value;

                                var from = bigMsg.Payload.Headers.Where(x => x.Name.ToLower() == "from").FirstOrDefault().Value;

                                subject = subject.Replace("FW:", "");
                                subject = subject.Replace("Ext:", "");
                                subject = subject.Replace("FW: -", "");
                                subject = subject.Trim();

                                Console.WriteLine("Processing message: " + subject);
    
                                int vendorID = 0;

                                bool hasTemplate = false;


                                string vendorName = "";

                                if (subject.ToLower().Contains("topco"))
                                {
                                    vendorID = Convert.ToInt32(Vendor.TOPCO);
                                    vendorName = "Topco Oilsite Products Limited";
                                    hasTemplate = true;
                                }
                                else if (subject.ToLower().Contains("gregg"))
                                {
                                    vendorID = Convert.ToInt32(Vendor.GREGG);
                                    vendorName = "Gregg Distributors LP";
                                    hasTemplate = true;

                                }
                                else if (subject.ToLower().Contains("badger"))
                                {

                                    vendorID = Convert.ToInt32(Vendor.BADGER);
                                    vendorName = "Badger Mining Corp.";
                                    hasTemplate = true;
                                }
                                else if(subject.ToLower().Contains("motion canada"))
                                {
                                    vendorID = Convert.ToInt32(Vendor.MOTIONCANADA);
                                    vendorName = "Motion Canada";
                                }
                                else if (subject.ToLower().Contains("mr. diesel"))
                                {
                                    vendorID = Convert.ToInt32(Vendor.MRDIESEL);
                                    vendorName = "Mr. Diesel";
                                }
                                else if (subject.ToLower().Contains("spm oil & gas canada ltd"))
                                {
                                    vendorID = Convert.ToInt32(Vendor.SPM);
                                    vendorName = "SPM Oil & Gas Canada Ltd";
                                }


                                string poNumberFromSplit = "";
                                string vendorFromSplit = "";
                                string invoiceNumberFromSplit = "";


                                if (vendorID == 0)
                                {

                                    try
                                    {

                                        if (subject.ToLower().Contains(" | "))
                                        {
                                            invoiceNumberFromSplit = subject.ToLower().Split(" | ")[1].Replace("'", "''").Trim();
                                            poNumberFromSplit = subject.ToLower().Split(" | ")[2].Replace("'", "''").Trim();
                                            vendorFromSplit = subject.ToLower().Split(" | ")[0].Replace("'", "''").Trim();
                                        }
                                        else
                                        {
                                            vendorFromSplit = subject;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        vendorFromSplit = subject;
                                    }
                                }

                                bool isOrder = false;
                                


                                if (vendorFromSplit.Length > 0)
                                {

                                    try
                                    {
                                        string sql = "select * from povendors where name like '%" + vendorFromSplit + "%' and isdeleted = 0";
                                        using (LocalDatabase ldb = new LocalDatabase(countryID))
                                        {
                                            ldb.myCommandLocal.CommandText = sql;
                                            ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();

                                            int vCount = 0;

                                 

                                            while (ldb.myReaderLocal.Read())
                                            {
                                                vendorID = Convert.ToInt32(ldb.myReaderLocal["id"].ToString());
                                                vCount++;

                                                isOrder = Convert.ToBoolean(Convert.ToInt32(ldb.myReaderLocal["showOnChem"].ToString())) || Convert.ToBoolean(Convert.ToInt32(ldb.myReaderLocal["showOnAcid"].ToString())) || Convert.ToBoolean(Convert.ToInt32(ldb.myReaderLocal["showOnSand"].ToString()));

                                            }

                                            if (vCount > 1)
                                                vendorID = 0;

                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }

                                }

                                int poID = 0;
                                int poVendorID = 0;

                                if (poNumberFromSplit.Length > 0 && isOrder == false)
                                {

                                    try
                                    {
                                        string sql = "select * from purchaseorders where poNumber = '" + poNumberFromSplit + "'";

                                        using (LocalDatabase ldb = new LocalDatabase(countryID))
                                        {
                                            ldb.myCommandLocal.CommandText = sql;

                                            ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();

                                            if (ldb.myReaderLocal.Read())
                                            {
                                                poID = Convert.ToInt32(ldb.myReaderLocal["id"].ToString());
                                                poVendorID = Convert.ToInt32(ldb.myReaderLocal["poVendorID"].ToString());
                                            }

                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }

                                if (poNumberFromSplit.Length > 0 && isOrder == true)
                                {

                                    try
                                    {
                                        string sql = "select * from orders where internalID = '" + poNumberFromSplit + "'";

                                        using (LocalDatabase ldb = new LocalDatabase(countryID))
                                        {
                                            ldb.myCommandLocal.CommandText = sql;

                                            ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();

                                            if (ldb.myReaderLocal.Read())
                                            {
                                                poID = Convert.ToInt32(ldb.myReaderLocal["id"].ToString());
                                                poVendorID = Convert.ToInt32(ldb.myReaderLocal["poVendorID"].ToString());
                                            }

                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }


                                string vendorIDStr = "null";

                                if (vendorID > 0)
                                    vendorIDStr = vendorID.ToString();


                                //if (vendorID > 0)
                                {

                                    //if (invID > 0)
                                    {

                                       

                                        foreach (var part in bigMsg.Payload.Parts)
                                        {
                                            if (!string.IsNullOrEmpty(part.Filename))
                                            {
                                                bool foundFile = false;
                                                int invID = 0;

                                                try
                                                {
                                                    var attachId = part.Body.AttachmentId;
                                                    MessagePartBody attachPart = service.Users.Messages.Attachments.Get("me", bigMsg.Id, attachId).Execute();
                                                    byte[] data = GetBytesFromPart(attachPart.Data);
                                                    string filePath = Path.Combine(fileFolder, $"{DateTime.Now:yyyyMMddHHmmss}-{vendorID}-{part.Filename.Replace("#", "")}");

                                                    var fSplit = part.Filename.Replace("#", "").Split(".");

                                                    File.WriteAllBytes(filePath, data);


                                                 

                                                    string prefixedInvoiceID = "EE";

                                                    using (LocalDatabase db = new LocalDatabase(countryID))
                                                    {
                                                        try
                                                        {


                                                            string sql = " SET nocount ON INSERT INTO invoices (poVendorID, invoiceStatusID, isDeleted, isCreated, createdBy, updatedBy, isOrder) values(" + vendorIDStr + ",  8, 0, 1, 'OCR', 'OCR', " + Convert.ToInt32(isOrder) + ") SET nocount OFF SELECT @@IDENTITY AS 'Identity'";

                                                            db.myCommandLocal.CommandText = sql;
                                                            db.myReaderLocal = db.myCommandLocal.ExecuteReader();


                                                            if (db.myReaderLocal.Read())
                                                            {
                                                                invID = Convert.ToInt32(db.myReaderLocal["Identity"].ToString());
                                                            }

                                                            db.myReaderLocal.Close();

                                                        }
                                                        catch (Exception e1)
                                                        {

                                                        }
                                                    }


                                                    using (LocalDatabase db = new LocalDatabase(countryID))
                                                    {
                                                        string sql = " update invoices set prefixedInvoiceID='" + prefixedInvoiceID + invID + "' where id = " + invID;

                                                        db.myCommandLocal.CommandText = sql;
                                                        db.myCommandLocal.ExecuteNonQuery();

                                                    }

                                                    if (invoiceNumberFromSplit.Length > 0)
                                                    {
                                                        using (LocalDatabase db = new LocalDatabase(countryID))
                                                        {
                                                            string sql = " update invoices set invoiceNumber='" + invoiceNumberFromSplit.Replace("''","'") + "' where id = " + invID;

                                                            db.myCommandLocal.CommandText = sql;
                                                            db.myCommandLocal.ExecuteNonQuery();

                                                        }
                                                    }


                                                    if (poID > 0 && isOrder == false)
                                                    {
                                                        using (LocalDatabase db = new LocalDatabase(countryID))
                                                        {
                                                            string sql = "insert into relInvoicePurchaseOrders(invoiceID, poID) values(" + invID + "," + poID + ")";
                                                            db.myCommandLocal.CommandText = sql;
                                                            db.myCommandLocal.ExecuteNonQuery();


                                                            //bring in my po parts
                                                       
                                                           // sql = "insert into purchaseorderparts(poID, isInvoice, partNumber, partDesc, equipmentID, qty, price, amount, afe,  afeString, afeID, glTypeID, pst, cost, dept, jobID, numLoads, mt, costPer, glCodeID, copiedFromOld, loadedAt, location, gst, regionID, invoiceDesc, spreadGLCode, spreadGLCodeID, shipmentItemID, standbyHrs, coilStringID, multiUnitPriceSplit, multiUnitExtendedSplit, sandHaulerLoadID )";
                                                           // sql = sql + " select " + invID + ", 1, partNumber, partDesc, equipmentID, qty,  price, amount, afe,  afeString, afeID, glTypeID, pst, cost, dept, jobID, numLoads, mt, costPer, glCodeID, copiedFromOld, loadedAt, location, gst, regionID, invoiceDesc, spreadGLCode, spreadGLCodeID, shipmentItemID, standbyHrs, coilStringID, multiUnitPriceSplit, multiUnitExtendedSplit, sandHaulerLoadID ";
                                                           // sql = sql + " from purchaseorderparts where poID= " + poID + " and isInvoice = 0 and isdeleted = 0 ";
                                                          //  db.insertOrUpdate(sql);

                                                        }
                                                    }
                                                    else if(poID > 0 && isOrder == true)
                                                    {
                                                        using (LocalDatabase db = new LocalDatabase(countryID))
                                                        {
                                                            string sql = "update invoices set orderID = " + poID + " where id= " + invID;
                                                            db.myCommandLocal.CommandText = sql;
                                                            db.myCommandLocal.ExecuteNonQuery();
                                                        }
                                                    }

                                                    using (LocalDatabase db = new LocalDatabase(countryID))
                                                    {
                                                        try
                                                        {
                                                            string sql = " INSERT INTO invoiceImages (invoiceID, filepath) values(" + invID + ", '" + filePath + "')";
                                                            db.myCommandLocal.CommandText = sql;
                                                            db.myCommandLocal.ExecuteNonQuery();

                                                            foundFile = true;
                                                        }
                                                        catch (Exception e1)
                                                        {

                                                        }
                                                    }

                                                    using (LocalDatabase db = new LocalDatabase(countryID))
                                                    {
                                                        try
                                                        {
                                                            string sql = " INSERT INTO GenericUploadedFiles (documentTypeID, filename,extension, createdByID, referenceID, referenceTable, adminID) values(25, '" + part.Filename.Replace("#", " ") + "','" + fSplit[1] + "',4," + invID + ",'Invoices', 4)";
                                                            db.myCommandLocal.CommandText = sql;
                                                            db.myCommandLocal.ExecuteNonQuery();

                                                            foundFile = true;
                                                        }
                                                        catch (Exception e1)
                                                        {

                                                        }
                                                    }

                                                    System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["invoiceGenericDocPath"] + "/" + invID);
                                                    System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["invoiceGenericDocPath"] + "/" + invID + "/4");

                                                    string genericDoc = ConfigurationManager.AppSettings["invoiceGenericDocPath"] + invID + "/4/" + part.Filename.Replace("#"," ");

                                                    File.WriteAllBytes(genericDoc, data);
                                                }
                                                catch (Exception noF)
                                                {

                                                }

                                                using (LocalDatabase db = new LocalDatabase(countryID))
                                                {
                                                    int statusID = 1;//new

                                                    if (!foundFile || vendorID == 0)
                                                        statusID = 3; //failed


                                                    bigMsg.LabelIds.Add(labelID);
                                                    ModifyMessageRequest modifyMessageRequest = new ModifyMessageRequest();
                                                    modifyMessageRequest.AddLabelIds = labels;
                                                    modifyMessageRequest.RemoveLabelIds = removeLabels;
                                                    service.Users.Messages.Modify(modifyMessageRequest, "me", bigMsg.Id).Execute();

                                                    try
                                                    {
                                                        string failedReason = "Invoice created.";

                                                        if(hasTemplate)
                                                        {
                                                            failedReason = "Waiting to process PDF.";
                                                        }

                                                        if (invID == 0)
                                                        {
                                                            failedReason = "Error reading email.";
                                                        }

                                                        if (!foundFile)
                                                        {
                                                            failedReason = "No PDF found.";
                                                        }


                                                        string sql = " INSERT INTO vendorInvoiceEmails (vendorID, subject, statusID, invoiceID, failedReason, fromEmail) values(" + vendorIDStr + ", '" + subject.Replace("'", "''") + "', " + statusID + ", " + invID + ", '" + failedReason + "', '" + from.Replace("'", "''") + "'   )";
                                                        db.myCommandLocal.CommandText = sql;
                                                        db.myCommandLocal.ExecuteNonQuery();


                                                    }
                                                    catch (Exception e1)
                                                    {
                                                        sendEmail("Element Invoice Email e1", e1.ToString());
                                                    }

                                                }

                                            }
                                        }

                                    }

                                }

                                Console.WriteLine("Email: " + subject + " done");


                                /*
                                else //no vendor found //no longer used as we put all invoices in
                                {
                                    bigMsg.LabelIds.Add(labelID);
                                    ModifyMessageRequest modifyMessageRequest = new ModifyMessageRequest();
                                    modifyMessageRequest.AddLabelIds = labels;
                                    modifyMessageRequest.RemoveLabelIds = removeLabels;
                                    service.Users.Messages.Modify(modifyMessageRequest, "me", bigMsg.Id).Execute();

                                    try
                                    {
                                        using (LocalDatabase db = new LocalDatabase(countryID))
                                        {
                                            string sql = " INSERT INTO vendorInvoiceEmails (vendorID, subject, statusID, invoiceID, failedReason, fromEmail) values(" + vendorID + ", '" + subject.Replace("'", "''") + "', 4, null , 'UNPROCESSED', '" + from.Replace("'", "''") + "')";
                                            db.myCommandLocal.CommandText = sql;
                                            db.myCommandLocal.ExecuteNonQuery();
                                        }
                                    }
                                    catch (Exception e1)
                                    {
                                        sendEmail("Element Invoice Email e1", e1.ToString());
                                    }

                                }*/

                            }
                            catch (Exception outer)
                            {
                                sendEmail("Element Invoice Email error1: " + subject, outer.ToString());
                            }
                        }
                    }

                    System.Threading.Thread.Sleep(900000);
                }


            }
            catch (Exception outerOuter)
            {
                sendEmail("Element Invoice Email error2", outerOuter.ToString());
            }
        }

        private static string DecodedString(string messagePart)
        {
            try
            {
                var data = GetBytesFromPart(messagePart);
                string decodedString = Encoding.UTF8.GetString(data);
                return decodedString;
            }
            catch (System.Exception e)
            {
                // ignored
                return string.Empty;
            }
        }

        private static byte[] GetBytesFromPart(string messagePart)
        {
            var attachData = messagePart.Replace('-', '+');
            attachData = attachData.Replace('_', '/');
            byte[] data = Convert.FromBase64String(attachData);
            return data;
        }

        public static DateTime UnixTimeStampToDateTime(long? _unixTimeStamp)
        {
            double unixTimeStamp = Convert.ToDouble(_unixTimeStamp);

            // Unix timestamp is seconds past epoch
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dateTime = dateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dateTime;
        }

        public static void sendEmail(string subject, string body, string to = "dave@ripplegroup.ca")
        {
            var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["smtpServer"])
            {
                Port = 587,
                Credentials = new NetworkCredential(ConfigurationManager.AppSettings["smtpLogin"], ConfigurationManager.AppSettings["smtpPassword"]),
                EnableSsl = true,
            };

            smtpClient.Send(ConfigurationManager.AppSettings["smtpLogin"], to, subject, body);
        }

    }

}
